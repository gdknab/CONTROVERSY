var cApp = angular.module('Account', ['angular-loading-bar', 'ngAnimate']);

cApp.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
	cfpLoadingBarProvider.includeSpinner = false;
}])

cApp.controller('IssuesController', function($scope, $http, $location, $anchorScroll) {
	$scope.issues_loaded = false;
	$http.get('https://issues.2pitau.org/controversy/issues/numopen')
	.success(function(res) {
    $scope.numopen = res.numopen;
		$scope.issues_loaded = true;
		if ($location.search().bug) {
			$location.hash('bugs');
			$anchorScroll();
		}
	})
	.error(function(res) {
		console.error('problem loading issues')	
	});
});
