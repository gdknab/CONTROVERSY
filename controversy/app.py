# -*- coding: utf-8 -*-
"""
    CONTROVERSY
    ~~~~~~~~~~~

    Controversy: joint mining of news text and
    social media to discover controversial points in news.
    
    Runs server.

    :copyright: (c) 2015--2018 LoDyShZh. Some rights reserved.
    :license: CC BY-NC-SA 4.0, see ../LICENSE for more details.
"""
from flask import Flask, session, redirect, render_template, request, Blueprint, flash, abort, Response
from jinja2 import TemplateNotFound
from functools import wraps
from api import api
from highlighting import highlighting 
from config import *
from datetime import datetime
from dateutil.relativedelta import relativedelta
import time
import db
import forms
from digest import digest
from error import UsageError

ANON_UNAME = 'bourbaki@illinois.edu'


application = Flask(__name__)

application.register_blueprint(api, url_prefix='/api')
application.register_blueprint(highlighting, url_prefix='/training')
application.secret_key = SECRET_KEY

application.config['RECAPTCHA_PUBLIC_KEY'] = CAPTCHA_PUBLIC
application.config['RECAPTCHA_PRIVATE_KEY'] = CAPTCHA_PRIVATE
application.config['version'] = '0.4'
application.config['testing'] = DEBUG


def plain_text_resp(message, code=200):
    return Response(message,
                    mimetype='text/plain',
                    status=code)


@application.errorhandler(404)
def handle_404(error):
    return plain_text_resp('not found', code=404)


@application.errorhandler(500)
def handle_500(error):
    application.logger.exception(error)
    from send_mail import send_mail 
    send_mail('500', repr(error))
    raise UsageError('our-fault', status_code=500)


def loggedin():
    return session.get('username') is not None


def require_login(view):
    @wraps(view)
    def protected_view(*args, **kwargs):
        if loggedin():
            return view(*args, **kwargs)
        else:
            return redirect('/login')
    return protected_view


@application.route('/bourbaki')
def bourbaki():
    username = ANON_UNAME 
    session['username'] = username
    session['user'] = db.dump_user(username)
    return redirect('/')

@application.route('/api-docs')
def api_docs():
    return render_template('docs.html')


@application.route('/login/<username>', methods=['GET', 'POST'])
@application.route('/login',
                   defaults={'username': None},
                   methods=['GET', 'POST'])
def login(username):
    clear_anon()

    if loggedin():
        return redirect('/')

    form = forms.Login()
    if form.validate_on_submit():
        session['username'] = form.username
        session['user'] = db.dump_user(form.username)
        return redirect('/')
    return render_template('login.html',
                           title='login',
                           form=form,
                           css=digest('css/login.css'),
                           username=username or '')



@application.route('/register', methods=['GET', 'POST'])
def register():
    form = forms.Register()
    if form.validate_on_submit():
        flash('thanks, %s; please confirm your password' % 
              first_name(form.name.data))
        return redirect('login/%s' % form.username)
    return render_template('register.html',
                           title='register',
                           form=form,
                           css=digest('css/login.css'),
                           logged_in = loggedin())


@application.route('/logout')
@require_login
def logout():
    u = session['username']
    session.pop('username', None)
    session.pop('user', None)
    flash('<b class=\'sans\'>%s</b>, you were logged out' % u)
    return redirect('/login')

def clear_anon():
    u = session.get('username')
    if u is not None:
        session.pop('username', None)
        session.pop('user', None)

@application.route('/register_anon')
def register_anon():
    clear_anon()
    return redirect('/register')

@application.route('/about')
@application.route('/account', methods=['GET', 'POST'])
@require_login
def account():
    form = forms.Login()
    if session['username'] != ANON_UNAME and form.validate_on_submit():
        db.drop_account(session['username'])
        flash('Account destroyed with vengeance!')
        return logout()
    return render_template('account.html',
                           title='about' if session['user']['Id'] == ANON_UNAME else 'account',
                           user=session['user'],
                           angular='Account',
                           js='static/js/account.js',
                           history=db.user_history(session['username']),
                           css=digest('css/account.css'),
                           form=form,
                           anon_uname=ANON_UNAME,
                           version=application.config['version'])


@application.route('/account/forget')
@require_login
def clear_queries():
    if session['username'] != ANON_UNAME:
        db.clear_queries(session['username'])
    return redirect('account')

    
@application.route('/partial/<path>')
@require_login
def serve_ang(path):
    try:
        return render_template('partials/%s' % path,
                               user=session['user'],
                               anon_uname=ANON_UNAME)
    except TemplateNotFound:
        return abort(404)


@application.route('/')
@require_login
def index():
    """all pages for the app are loaded through here.
    Angular handles partials, which are rendered through ``/partial``
    """
    return render_template('app.html',
            user=session['user'],
            css=digest('css/app.css'),
            js=digest('js/home.js'),
            angular='Home')


@application.route('/not-supported')
def not_supported():
    return render_template('not-supported.html')


@application.template_filter('first_name')
def first_name(s):
    return s.split(' ')[0]


def clean_rd(rd):
    """relative delta --> nice string
    """
    attrs = ['years', 'months', 'days', 'hours', 'minutes', 'seconds']
    res = []
    res_append = res.append
    for a in attrs:
        if getattr(rd, a) and getattr(rd, a) > 1:
            res_append('%d %s' % (getattr(rd, a), a))
    return res[0] if len(res) else 'a second'


@application.template_filter('pretty_date')
def pretty_date(u):
    """mysql timestamp --> nice strftime + " ... ago"
    """
    now = datetime.fromtimestamp(time.time())
    then_secs = (u - datetime(1970, 1, 1)).total_seconds()
    then = datetime.fromtimestamp(then_secs)
    delta = clean_rd(relativedelta(now, then))
    return '%s ago' % delta, then.strftime('%A, %d %B')


if __name__ == '__main__':
    application.run(host='0.0.0.0',
                    port=4041,
                    debug=DEBUG)
