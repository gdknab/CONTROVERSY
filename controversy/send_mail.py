from config import SMTP_BOT_SENDER, SMTP_BOT_SENDER_PW, ADMIN_EMAIL, APP_NAME
import smtplib
from email.mime.text import MIMEText
from datetime import datetime
import pytz

SMTP_SERVER = 'smtp.2pitau.org'
SMTP_PORT = 587

def send_mail(subj, body):
    midwest = pytz.timezone('America/Chicago')
    date = datetime.now(midwest)

    msg = MIMEText('''%s

timestamp: %s in Chicago
(you're receiving this from a 2PITAU mailbot. Forward this message to postmaster@2pitau.org to unsubscribe.)''' % 
                   (body, datetime.strftime(date, '%a, %d %B %Y, %H:%M')))

    msg['Subject'] = '[%s] %s' % (APP_NAME, subj)
    msg['From'] = SMTP_BOT_SENDER
    msg['To'] = ADMIN_EMAIL 

    smtp = smtplib.SMTP(SMTP_SERVER, SMTP_PORT)
    smtp.login(msg['From'], SMTP_BOT_SENDER_PW)

    ret = True 
    try:
        smtp.sendmail(msg['From'], msg['To'], msg.as_string())
    except:
        ret = False
    finally:
        smtp.quit()
        return ret


    
